checked = false;

/** JS Function checkall checkbox **/
function checkall(form){
    if (checked == false){
        checked = true;
    }else{
        checked = false;
    }
    $chk_count = document.getElementById(form).elements.length;
    for(var i=0; i < $chk_count; i++){
        document.getElementById(form).elements[i].checked = checked;
    }
}

/** JS Function confirm delete data **/
function confirm_delete(url){
    var result = confirm("Delete selected data ???");
    if (result == true){
        if (url != ""){
            document.writeln(url);
            document.location.href = url;
        }
        return true;
    }else{
        return false;
    } 
}

function redirect(url){
    document.location.href = url;
}

function show_notif_info(message, caption){			
	$.Notify({
		style: {
			background: 'black', 
			color: 'white'
			},
		caption: caption,
		shadow: true,
		content: '<b>'+message+'</b>',
		timeout : 5000,
	});
}

function show_notif_warning(message, caption,duration){			
	var to = 0;
	if(duration == ""){
		to = 5000;
	}else{
		to = duration;
	}
	
	$.Notify({
		style: {
			background: 'red', 
			color: 'white'
			},
		caption: caption,
		shadow: true,
		content: '<b>'+message+'</b>',
		timeout : to,
	});
}

function show_notif(message, caption,duration){			
	$.Notify({
		style: {
			background: '#1ba1e2', 
			color: 'white'
			},
		caption: caption,
		shadow: true,
		content: '<b>'+message+'</b>',
		timeout : duration,
	});
}

function date_today(){
	var tanggal = new Date();
	var bln = tanggal.getMonth()+1;
	if (bln < 10){
		bln = "0"+(tanggal.getMonth()+1);
	}
	return tanggal.getFullYear() + "-" + bln + "-" + tanggal.getDate();
}

function CreateAJAX(){ 
	if (window.XMLHttpRequest){ 
		return new XMLHttpRequest(); 
	} 
	if (window.ActiveXObject){ 
		return new ActiveXObject("Microsoft.XMLHTTP"); 
	} 
	return null; 
}