﻿$(document).ready(function() {
    oTable =$('#datagrid').dataTable({
		//"aaSorting": [[ 1, "asc" ]],
		"aLengthMenu": [[5,10, 25, 50,100, -1], [5,10, 25, 50,100, "All"]],
		
		"sPaginationType": "full_numbers",
		"sDom": 'T<"clear"><"toolbar">frltip',
		//"sDom": 'T<"clear">lfrtip',
		/*"bStateSave": true,*/
		
		"sScrollY": "300px",
			"bPaginate": true,
			"bScrollCollapse": true,
		
		"oLanguage": {
			"sLengthMenu": "Display _MENU_",
			"sInfoFiltered": "(Search from _MAX_ total records)",
			"sInfo": "Showing : <b>_START_ to _END_ of _TOTAL_ </b>records"
		},
	
		"oTableTools": {
		//"sRowSelect": "multi",
			"sSwfPath": "../system/swf/copy_csv_xls_pdf.swf",
			
			"aButtons": [
			//"select_all", "select_none"
			{
				"sExtends":    "copy",
				"sButtonText": "Copy to Clipboard",
				
			},
			
			"print",{
				"sExtends":    "collection",
				"sButtonText": "Export To ..",
				"aButtons":    [ "csv","xls",
						{
						"sExtends": "pdf",
						"sPdfOrientation": "landscape",
						"sPdfMessage": "Data Barang",
						
						}
						]
				}
			],
		}

    });
	
	/*** For Transaction ***/
	oTable =$('#gridtrans').dataTable({
		
		"sDom": '<"clear"><"toolbar">fr',
		/*"bStateSave": true,*/
		
		"sScrollY": "300px",
			"bPaginate": true,
			"bScrollCollapse": false,
		
		"oLanguage": {
			"sLengthMenu": "Display _MENU_",
			"sInfoFiltered": "(Search from _MAX_ total records)",
			"sInfo": "Showing : <b>_START_ to _END_ of _TOTAL_ </b>records"
		}

    });
} );